/**
 * Created by qian on 17-8-15.
 */
var itemPerPage = 20;
var apiURL='/api/v1';
var timeoutNum = 10*1000;

$(document).ready(function() {
    // window.addEventListener("popstate", function(event){
    //     var currentState = event.state;
    //     if(currentState && currentState.pageIndex){
    //         var pageIndex = currentState.pageIndex;
    //         console.log("move to another page " + pageIndex);
    //     }
    //     console.log("history.length "+ history.length);
    //     getHomePageData(pageIndex, true);
    // });
    // var currentState = history.state;
    // if(currentState && currentState.pageIndex){
    //     getHomePageData(currentState.pageIndex, true);
    // }else{
    //     window.history.replaceState({pageIndex: 1}, null, "");
        getHomePageData(1, true);
    // }
});

$('#search-form').submit(function (event) {
    event.preventDefault();
    var query = $('#question').val();
    var threshold = $('#search-threshold').val();
    $('#viewNodePaginationTop').remove();
    $('#accordion').empty();

    if (query != '') {
        $.post("/search_ajax", {'query': query, 'threshold': threshold}, function (data) {
            $('#accordion').html(data);
        });
    } else {
        getHomePageData(1, true);
    }
})

$('#submit').click(function () {
    var q = $('#question').val();
    var a = $('#answer').val();
    var name = $('#username').val();
    var email = $('#email').val();
    var threshold = $('#threshold').val() || '0.5'
    $.post("/api/v1/add", {'q': q, 'a': a, 'name': name, 'email': email, 'threshold': threshold})
    window.location.replace("/")
})

$('#upload').click(function () {
    $('#accordion').html('');
    $('#msg').html('')
    $('#ajax_loader').show();
    var fileData = new FormData($('#uploadForm')[0])
    $.ajax({
        url: '/upload',
        type: 'post',
        data: fileData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('#ajax_loader').hide();
            // window.location.replace("/")
            getHomePageData(1, true);
        },
        error: function (error) {
            $('#ajax_loader').hide();
            $('#msg').append(error.status + ': ' + error.statusText + '<br>Upload failed!');
        }
    })
})

$('#clear').click(function () {
    $.get({
        url: '/api/v1/clear',
        success: function () {
            window.location.replace("/")
        }
    })
})

function getHomePageData(pageIndex, flag){
    //在页面点击分页链接时，额外添加history entry，用于页面跳转。其他情况只获取分页数据。
    if(!flag){
        window.history.pushState({pageIndex: pageIndex}, null, "");
    }
    var _request_data = new Object();

    var _user_id = $('#user_id').html();
    if(_user_id!='undefined')
       _request_data.user_id = _user_id;


    $('#accordion').html('');
    $('#msg').html('');
    var start = (pageIndex-1) * itemPerPage+1;
    var end = start + itemPerPage -1;

    $('#ajax_loader').show()

    $.ajax( {
        type: 'GET',
        url: apiURL + "/get_index_results?start="+start+"&end=" + end,
        // data:_request_data,
        dataType: "json",
        // 解决ie10下, issue#85,
        //a node list screen becomes improper after save, delete or copy a curation node with IE10
        // add cache:false
        cache:false,
        timeout: timeoutNum,
        error: function(err){
            //连接超时处理
            // if(err.statusText == "timeout"){
            //     $("#byUpdateAjaxLoader").hide();
            //     $("#tabs-latest").find("#connectionTimeout_prompt").show();
            // }
            return;
        },
        success: function(data) {
            $('#ajax_loader').hide()
            if(data.count <= 0){
                $('#msg').html('No results');
                return;
            }
            // showCuration(data,itemPerPage,pageIndex);
            // console.log(data);
            $('#accordion').html(data.results);
            createPagenationStr4Search(data.db_count, itemPerPage, pageIndex, "getHomePageData");
        }
    });
}

/**
 * 显示Curation
 */
function showCuration(data,iCPP,pageIndex){
    $("#AjaxLoader").hide();
    if(data.length == 0){
        $("#search_prompt").show();
        return;
    }

    var user_name = $('#username').html();
    //if(user_name === 'undefined'){}
    user_name = 'public';

    console.log(apiURL + "/count/nodes/" + user_name);
    $.ajax({
        type: 'GET',
        url: apiURL + "/count/nodes/" + user_name,
        dataType: "json",
        data: {"isSelf":"true","tagNameList":clickFilterTagList,"authorList":clickFilterAuthorList},
        cache: false,
        success: function(data) {
            createPagenationStr4Search(data.numCount,iCPP,pageIndex,"getHomePageData");
        },
        error:function(err){
            console.log(err);
        }
    });
    showData(data);
}

/**
 * 创建分页的方法
 */
function createPagenationStr4Search(count,numPerPage,pageIndex,callback){
    $("#viewNodePaginationTop").html("");
    $("#viewNodePaginationTop").append("<ul></ul>");
    // 数据库总数目没达到一页大小时，不分页
    if(pageIndex == 1){
        if(count <=numPerPage){
            $("#viewNodePaginationTop").hide();
            return;
        }else{
            $("#viewNodePaginationTop").show();
        }
    }

    var largest_page = Math.ceil(count/numPerPage);

    var page_num = 10;
    var start_page = Math.ceil(pageIndex/page_num)*page_num - page_num +1,
        end_page = start_page + page_num -1;

    if(end_page > largest_page){
        end_page = largest_page;
        start_page = end_page - page_num +1;
        if(start_page < 1)
            start_page = 1;
    }

    if(pageIndex > 1)
        $("#viewNodePaginationTop").append("<li class='prev'><a href='javascript:void(0)' onclick='"+callback+"(" + (pageIndex - 1) + ")'>Prev</a></li>");
    else
        $("#viewNodePaginationTop").append("<li class='prev disabled'><a>Prev</a></li>");

    for(var i=start_page; i<=end_page; i++){
        if(i === pageIndex)
            $("#viewNodePaginationTop").append("<li class='active'><a href='javascript:void(0)' onclick='"+callback+"(" + i + ")'>"+i+"</a></li>");
        else
            $("#viewNodePaginationTop").append("<li><a href='javascript:void(0)' onclick='"+callback+"(" + i + ")'>"+i+"</a></li>");
    }

    if(pageIndex < largest_page)
        $("#viewNodePaginationTop").append("<li class='next'><a href='javascript:void(0)' onclick='"+callback+"(" + ( pageIndex + 1) + ")'>Next</a></li>");
    else
        $("#viewNodePaginationTop").append("<li class='next disabled'><a>Next</a></li>");
}
