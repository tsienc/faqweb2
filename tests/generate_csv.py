from pymongo import MongoClient
client = MongoClient(host='localhost')
db = client['new_db']

import csv

qas = db.qas.find()
with open('data.csv', 'w') as f:
    fieldsnames = ['question', 'answer', 'username', 'emailaddress']
    writer = csv.DictWriter(f, fieldnames=fieldsnames)
    writer.writeheader()
    for qa in qas:
        writer.writerow({'question': qa['question'], 'answer': qa['answer'], 'username':qa['username'], 'emailaddress': qa['emailaddress']})


f.close()
client.close()
