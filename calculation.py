from preprocessing import text_preprocess, get_parsed_words
from app import db
from bson.objectid import ObjectId
import math


def search_similar_questions(question, threshold=0.9, limit=10):
    question = text_preprocess(question)
    similar_questions = []
    for qa in db.qas.find():
        similarity = cosine_similarity(question, qa['question'])
        similar_questions.append((str(qa['_id']), similarity, qa['question'], qa['answer']))

    similar_questions.sort(key=lambda p: p[1], reverse=True)
    limit = limit if limit < len(similar_questions) else len(similar_questions)
    index = 0
    while index < limit and similar_questions[index][1] >= threshold:
        index += 1
    similar_questions = similar_questions[0:index]

    return similar_questions


def update_question_answer(index, question, answer, username, emailaddress):
    question, answer = text_preprocess(question), text_preprocess(answer)
    db.qas.update_one({'_id': ObjectId(index)}, {'$set': {'question': question, 'answer': answer, 'username': username, 'emailaddress': emailaddress}})


def insert_question_answer(question, answer, username, emailaddress):
    db.qas.insert({'question': question, 'answer': answer, 'username': username, 'emailaddress': emailaddress})


def delete_question_answer(_id):
    db.qas.delete_one({'_id': ObjectId(_id)})
    pass


def doc2vec(doc):
    words = get_parsed_words(doc)
    vec = {}
    keys = set(words)
    for key in keys:
        vec[key] = words.count(key)
    return vec


def cosine_similarity(doc1, doc2):
    vec1, vec2 = doc2vec(doc1), doc2vec(doc2)
    if not (vec1 and vec2):
        return 0
    product, len1, len2 = 0, 0, 0
    for word in vec1:
        count1 = vec1[word]
        if word in vec2:
            count2 = vec2[word]
            vec2.pop(word)
        else:
            count2 = 0
        product += count1 * count2
        len1 += count1 * count1
        len2 += count2 * count2
    for word in vec2:
        count2 = vec2[word]
        len2 += count2 * count2
    return product / math.sqrt(len1*len2)



