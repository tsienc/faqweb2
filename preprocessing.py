import unicodedata, re

from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer


def remove_html_elements(string):
    cleaner = re.compile('<.*?>')
    cleantext = re.sub(cleaner, ' ', string)
    return cleantext


def strip_text(string):
    return string.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ').strip()


def text_preprocess(string):
    return remove_html_elements(strip_text(string))


def is_wanted_unicode_type(tokens):
    for token in tokens:
        category = unicodedata.category(token)
        if category.startswith('L'):
            return True
        else:
            continue
    return False


def parsed_post_process(parsed):
    tokens=[word.lower() for word in word_tokenize(parsed)]
    # remove stop words
    # english_stopwords = stopwords.words('english')
    # tokens=[word for word in tokens if word not in english_stopwords]
    # # remove punctuation
    # english_punctuations = [',', '.', ':', ';', '?', '(', ')', '[', ']', '&', '!', '*', '@', '#', '$', '%']
    # tokens=[word for word in tokens if word not in english_punctuations]
    # # stemming
    # st = LancasterStemmer()
    # tokens=[st.stem(word) for word in tokens]

    parsed_list = [parsed_word.lower() for parsed_word in tokens if is_wanted_unicode_type(parsed_word)]
    return parsed_list


def get_parsed_words(string):
    string = text_preprocess(string)
    parsed_list = parsed_post_process(string)
    return parsed_list
