from flask import request, jsonify, render_template, redirect, url_for
from app import app, db

import os
import csv

from calculation import search_similar_questions, update_question_answer, insert_question_answer, delete_question_answer



@app.route('/')
def index():
    qas = db.qas.find()
    return render_template('index.html', qas=qas)


@app.route('/delete/<_id>')
def delete(_id):
    delete_question_answer(_id)
    return redirect(url_for('index'))


@app.route('/search_ajax', methods=['POST'])
def search_ajax():
    query = request.form['query']
    threshold = float(request.form['threshold'])
    top_10 = search_similar_questions(query, threshold, 10)
    qas = []
    for qa in top_10:
        qas.append({'question': qa[2], 'answer': qa[3]})
    return render_template('result.html', qas=qas)


@app.route('/api/v1/check_dup', methods=['GET', 'POST'])
def api_check_duplicate():
    if request.method == 'GET':
        question = request.args.get('q')
        answer = request.args.get('a')
        threshold = float(request.args.get('threshold'))
    else:
        question = request.form.get('q')
        answer = request.form.get('a')
        threshold = float(request.form.get('threshold'))

    qas = search_similar_questions(question, threshold)
    response = {}
    if qas:
        response['msg'] = 'found similar question-answers'
        response['qas'] = []
        for qa in qas:
            response['qas'].append({'index': qa[0], 'similarity': qa[1], 'question': qa[2], 'answer': qa[3]})
    else:
        response = {'msg': 'no similar question-answers'}

    return jsonify(response)


@app.route('/api/v1/add', methods=['GET', 'POST'])
def api_add():
    if request.method == 'GET':
        question = request.args.get('q')
        answer = request.args.get('a')
        username = request.args.get('name')
        emailaddress = request.args.get('email')
        threshold = float(request.args.get('threshold'))
    else:
        question = request.form.get('q')
        answer = request.form.get('a', 10)
        username = request.form.get('name')
        emailaddress = request.form.get('email')
        threshold = float(request.form.get('threshold'))

    qas = search_similar_questions(question, threshold)
    response = {}
    if qas:
        response['msg'] = 'found similar question-answers'
        response['qas'] = []
        for qa in qas:
            response['qas'].append({'index': qa[0], 'similarity': qa[1], 'question': qa[2], 'answer': qa[3]})
    else:
        insert_question_answer(question, answer, username, emailaddress)
        response = {'msg': 'has inserted'}

    return jsonify(response)


@app.route('/api/v1/update', methods=['GET', 'POST'])
def api_update():
    if request.method == 'GET':
        index = request.args.get('index')
        q = request.args.get('q')
        a = request.args.get('a')
        username = request.args.get('name')
        emailaddress = request.args.get('email')
    else:
        index = request.form.get('index')
        q = request.form.get('q')
        a = request.form.get('a', 10)
        username = request.form.get('name')
        emailaddress = request.form.get('email')
    update_question_answer(index, q, a, username, emailaddress)
    return jsonify({'status': 'success'})


@app.route('/api/v1/get_count', methods=['GET', 'POST'])
def api_get_count():
    count = db.qas.count()
    return jsonify({"count": count})


@app.route('/api/v1/get_qas', methods=['GET', 'POST'])
def api_get_qas():
    if request.method == 'GET':
        start = request.args.get('start')
        end = request.args.get('end', int(start) + 10)
    else:
        start = request.form.get('start')
        end = request.form.get('end', int(start) + 10)
    response = {}
    start, end = int(start) - 1, int(end)
    count = db.qas.count()
    if start > count:
        start = count
    if end > count:
        end = count
    response['count'] = end - start
    response['results'] = []
    qas = db.qas.find()[start:end]
    for index, qa in enumerate(qas):
        _id = str(start + index + 1)
        response['results'].append({'id': _id, 'question': qa['question'], 'answer': qa['answer']})

    return jsonify(response)


@app.route('/api/v1/get_index_results', methods=['GET', 'POST'])
def api_get_index_results():
    if request.method == 'GET':
        start = request.args.get('start')
        end = request.args.get('end', int(start) + 10)
    else:
        start = request.form.get('start')
        end = request.form.get('end', int(start) + 10)
    response = {}
    start, end = int(start) - 1, int(end)
    count = db.qas.count()
    if start > count:
        start = count
    if end > count:
        end = count
    response['count'] = end - start
    response['db_count'] = db.qas.count()
    qas = db.qas.find().skip(start).limit(end - start)
    # for _index, qa in enumerate(qas):
    #     qa['id'] = _index + start + 1
    response['results'] = render_template('index_results.html', qas=qas, start=start)
    return jsonify(response)


@app.route('/api/v1/clear', methods=['GET', 'POST'])
def api_clear_data():
    db.qas.remove()
    response = {}
    response['msg'] = 'clear succeed'
    return jsonify(response)


UPLOAD_FOLDER = 'upload'
project_dir = os.path.dirname(os.path.abspath(__file__))
app.config['UPLOAD_FOLDER'] = os.path.join(project_dir, UPLOAD_FOLDER)

# to depress "_csv.Error: field larger than field limit (131072)"
# refer https://stackoverflow.com/questions/15063936/csv-error-field-larger-than-field-limit-131072
import sys
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.

    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True


@app.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['datafile']
    if file:
        filename = file.filename
        saved_filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(saved_filename)
        file.close()
        saved_file = open(saved_filename, encoding='utf8')
        reader = csv.DictReader(saved_file)
        qas = []
        for row in reader:
            qas.append({'question': row['question'], 'answer': row['answer'], 'username': row['username'], 'emailaddress': row['emailaddress']})
        db.qas.insert_many(qas)
        saved_file.close()
        # os.remove(saved_filename)
        return 'Upload Successfully'
    else:
        return 'Upload Failed'


app.run(host='0.0.0.0', port=5002, debug=True)
