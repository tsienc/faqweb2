from flask import Flask
from flask_bootstrap import Bootstrap
from flask_pymongo import PyMongo

import config

from pymongo import MongoClient



app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config['MONGO_HOST'] = config.mongo_host
app.config['MONGO_DBNAME'] = config.mongo_dbname
Bootstrap(app)
# db = PyMongo(app).db
db = MongoClient(host=config.mongo_host)[config.mongo_dbname]

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002, debug=True)