*Faqweb* is the server application for Outlook Plug-in. You can post your question and answer to this website.
You can also search the website by typing in a question to get a list of possible answers.

# Deployment
1. install required packages in requirements.txt
2. change database configurations in config.py
3. change the host and ip at the end of views.py
4. run view.py
